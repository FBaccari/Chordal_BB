{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction\n",
    "\n",
    "This notebook is the computational appendix of [arXiv:1808.01275](https://arxiv.org/abs/1808.01275). We demonstrate how to use a version of branch and bound algorithm, augmentend with the chordal extension technique , to compute the exact ground state energy of classical spin models. We also give the details how to reproduce the numerical results shown in the paper. \n",
    "To improve readability of this notebook, we placed the supporting functions to two separate files; please download these in the same folder as the notebook if you would like to evaluate it. The following dependencies must also be available: at least one SDP solver ([SDPA](http://sdpa.sourceforge.net) as an executable in the path or [Mosek](https://mosek.com) with its Python interface installed; cvxopt as a solver is not recommended) together with the [Ncpol2sdpa](http://pypi.python.org/pypi/ncpol2sdpa), [networkx](https://networkx.github.io), [dwavenetworkx](http://dwave-networkx.readthedocs.io/en/latest/index.html) and [chompack](https://chompack.readthedocs.io/en/latest/) packages.\n",
    "First, we import everything we will need:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from branchandbound_tools import *\n",
    "from sympy import S\n",
    "from spin_models import *\n",
    "import numpy as np\n",
    "import networkx as nx\n",
    "import dwave_networkx as dnx\n",
    "import chompack\n",
    "from ncpol2sdpa.chordal_extension import find_variable_cliques\n",
    "from ncpol2sdpa import generate_variables, flatten, SdpRelaxation, get_monomials"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also set the precision for the SDP solver"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "_precision = 10**(-5)\n",
    "solverparameters = {\n",
    "    'dparam.intpnt_co_tol_rel_gap': _precision,\n",
    "    'dparam.intpnt_co_tol_mu_red': _precision,\n",
    "    'dparam.intpnt_nl_tol_rel_gap': _precision,\n",
    "    'dparam.intpnt_nl_tol_mu_red': _precision,\n",
    "    'dparam.intpnt_tol_rel_gap': _precision,\n",
    "    'dparam.intpnt_tol_mu_red': _precision,\n",
    "    'dparam.intpnt_co_tol_dfeas': _precision,\n",
    "    'dparam.intpnt_co_tol_infeas': _precision,\n",
    "    'dparam.intpnt_co_tol_pfeas': _precision,\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the next, we will be interested in finding the ground state energy and configuration for Ising spin $\\frac{1}{2}$ models of the form\n",
    "\n",
    "\\begin{equation}\n",
    "\\mathcal{H} = - \\sum_{\\langle i,j \\rangle} \\sigma_i \\sigma_j + \\sum_i h_i \\sigma_i \n",
    "\\end{equation}\n",
    "\n",
    "where the first sum runs over pair of interacting spins $\\langle i,j \\rangle$ distributed according to some topology. The couplings are set to be ferromagentic, while the local fields $h_i$ are chosen randomly from a Gaussian distribution with zero mean and variance $\\sigma$.\n",
    "As a function of the disorder strength $\\sigma$ the model undergoes a phase transition from a ferromagnetic ground state (in which all states are aligned with each other) to a disordered phase (in which, for extremely large disorder, the spins are aligned with the local magnetic fields).\n",
    "We set $\\sigma = 1.5$ to be close to the phase transition but slightly on the disordered phase"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sigma = 1.5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the following we will show how to apply the CBB method to different topologies, namely a 2D square lattice, a triangular lattice and a Chimera graph.\n",
    "For all of them it is convenient to introduce the two parameters parameters $l,k$ for a $l\\times k$ lattice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "l = 5\n",
    "k = 5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2D square ferromagnetic Ising model with random mangetic field\n",
    "\n",
    "We first generate the spin variables together with their operator properties"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "configuration = [2]\n",
    "s_variables = get_square2D_spins(k, l, configuration)\n",
    "substitutions = {M**2: S.One for M in flatten(s_variables)}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then choose an instance of random local fields and define the hamiltonian"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-1.0*A0*B0 - 1.0*A0*F0 - 4.07414362713386*A0 - 1.0*B0*C0 - 1.0*B0*G0 - 2.52535019634295*B0 - 1.0*C0*D0 - 1.0*C0*H0 + 1.79141559395009*C0 - 1.0*D0*E0 - 1.0*D0*I0 - 2.58847870213927*D0 - 1.0*E0*J0 + 0.274741410294459*E0 - 1.0*F0*G0 - 1.0*F0*K0 + 0.834407931686358*F0 - 1.0*G0*H0 - 1.0*G0*L0 + 0.971041331704162*G0 - 1.0*H0*I0 - 1.0*H0*M0 - 0.549539973320524*H0 - 1.0*I0*J0 - 1.0*I0*N0 + 1.28082828560965*I0 - 1.0*J0*O0 - 0.780831244902699*J0 - 1.0*K0*L0 - 1.0*K0*P0 + 0.629640865275891*K0 - 1.0*L0*M0 - 1.0*L0*Q0 - 1.66209969800955*L0 - 1.0*M0*N0 - 1.0*M0*R0 + 0.686097067341404*M0 - 1.0*N0*O0 - 1.0*N0*S0 + 3.25924169632185*N0 - 1.0*O0*T0 - 1.29177550116773*O0 - 1.0*P0*Q0 - 1.0*P0*U0 + 2.31769201814183*P0 - 1.0*Q0*R0 - 1.0*Q0*V0 - 1.85367757222922*Q0 - 1.0*R0*S0 - 1.0*R0*W0 + 1.3765943097099*R0 - 1.0*S0*T0 - 1.0*S0*X0 + 1.42309268447472*S0 - 1.0*T0*Y0 - 0.818643344811832*T0 - 1.0*U0*V0 - 0.496966781123205*U0 - 1.0*V0*W0 - 2.0571148998314*V0 - 1.0*W0*X0 - 1.04737547173743*W0 - 1.0*X0*Y0 + 0.174460371377641*X0 - 0.773657216518318*Y0\n"
     ]
    }
   ],
   "source": [
    "local = get_2Dsquare_localdisorder(k, l, sigma)\n",
    "hamiltonian = get_2Dsquare_ferromagneticdisorder_hamiltonian(k,l,local, s_variables)\n",
    "print(hamiltonian)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Extract the cliques from the sparsity structure of $\\mathcal{H}$ with the chordal extension method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[T0, X0, Y0], [S0, T0, W0, X0], [O0, S0, T0, W0], [P0, U0, V0], [P0, Q0, V0, W0], [K0, P0, Q0, W0], [K0, L0, Q0, R0, W0], [H0, L0, M0, N0, R0], [A0, B0, F0], [B0, F0, G0, K0], [B0, C0, G0, K0], [C0, G0, H0, K0, L0], [D0, E0, J0], [D0, I0, J0, O0], [C0, D0, I0, O0], [C0, H0, I0, N0, O0], [C0, H0, K0, L0, N0, O0], [H0, K0, L0, N0, O0, R0], [K0, L0, N0, O0, R0, W0], [N0, O0, R0, S0, W0]]\n"
     ]
    }
   ],
   "source": [
    "cliques = find_variable_cliques(flatten(s_variables), hamiltonian)\n",
    "print(cliques)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set a threshold size $n_t$ for the hybrid level of the hierarcy and run the CBB"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Starting initial optimisation\n",
      "The lower bound is -48.32517996704147\n",
      "The upper bound is -42.0358599915542\n",
      "The lower bound is -45.50908871401742\n",
      "Time taken for lower is 3.4177348613739014\n",
      "Time taken for upper is 0.6373717784881592\n",
      "The lower bound is -44.44794050219821\n",
      "Time taken for lower is 3.099591016769409\n",
      "Time taken for upper is 0.6733758449554443\n",
      "The upper bound is -45.5004006633800\n",
      "The lower bound is -45.50050413289574\n",
      "Time taken for lower is 2.969590902328491\n",
      "Time taken for upper is 0.5779461860656738\n",
      "The lower bound is -43.107827236301986\n",
      "Time taken for lower is 3.8374948501586914\n",
      "Time taken for upper is 0.622495174407959\n",
      "The upper bound is -45.5004006633800\n"
     ]
    }
   ],
   "source": [
    "threshold = 3\n",
    "[z_low,z_up,final_config] = get_groundBandB(s_variables,substitutions,hamiltonian,\n",
    "                                            cliques,threshold,\n",
    "                                            solverparameters,verbose = 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2D triangular ferromagnetic Ising model with random mangetic field\n",
    "\n",
    "We strat by generating the spin variables, this time distributed according to a triangular lattice. We make use of the networkx lattice generator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "lattice = nx.generators.lattice.triangular_lattice_graph(k, l)\n",
    "configuration = [2]\n",
    "s_variables = get_lattice_spins(lattice, configuration)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then choose an instance of random local fields and define the hamiltonian"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-A0*B0 - A0*E0 - 1.74970936834207*A0 - B0*C0 - B0*E0 - B0*F0 + 3.51857616652405*B0 - C0*D0 - C0*F0 - C0*G0 - 2.08278871293718*C0 - D0*G0 - 0.66085979500981*D0 - E0*F0 - E0*H0 - E0*I0 + 2.33225981367055*E0 - F0*G0 - F0*I0 - F0*J0 + 0.132836941590065*F0 - G0*J0 - G0*K0 - 0.595170924512771*G0 - H0*I0 - H0*L0 + 0.740197355895547*H0 - I0*J0 - I0*L0 - I0*M0 - 0.729750360074339*I0 - J0*K0 - J0*M0 - J0*N0 + 0.639428156076313*J0 - K0*N0 - 1.31687378129981*K0 - L0*M0 - L0*O0 - L0*P0 + 1.83085269687955*L0 - M0*N0 - M0*P0 - M0*Q0 - 0.117891577740305*M0 - N0*Q0 - N0*R0 - 2.20882825141204*N0 - O0*P0 - O0*S0 - 1.85692161668954*O0 - P0*Q0 - P0*S0 - P0*T0 + 0.556987092666827*P0 - Q0*R0 - Q0*T0 - Q0*U0 + 1.48125502162854*Q0 - R0*U0 + 1.29724253762136*R0 - S0*T0 + 2.28416717500848*S0 - T0*U0 - 2.67711039418402*T0 + 1.07048132014825*U0\n"
     ]
    }
   ],
   "source": [
    "h = get_lattice_localdisorder(lattice,sigma)\n",
    "J = get_lattice_ferromangetic(lattice)   \n",
    "hamiltonian = get_lattice_hamiltonian(lattice,h,J,s_variables)\n",
    "print(hamiltonian)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Extract the cliques from the sparsity structure of $\\mathcal{H}$ with the chordal extension method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[O0, P0, S0, T0], [L0, O0, P0, T0], [Q0, R0, T0, U0], [N0, Q0, R0, T0], [G0, J0, K0, N0], [C0, D0, G0], [A0, B0, E0], [B0, C0, E0, F0], [C0, E0, F0, G0], [E0, H0, I0, L0], [E0, F0, G0, I0, L0], [F0, G0, I0, J0, L0], [G0, I0, J0, L0, N0], [I0, J0, L0, M0, N0], [L0, M0, N0, P0, Q0], [L0, N0, P0, Q0, T0]]\n"
     ]
    }
   ],
   "source": [
    "variables = [el[0][0] for el in s_variables.values()]\n",
    "cliques = find_variable_cliques(variables, hamiltonian)\n",
    "print(cliques)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set a threshold size $n_t$ for the hybrid level of the hierarcy and run the CBB"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Starting initial optimisation\n",
      "The lower bound is -49.99864665823647\n",
      "The upper bound is -40.3312656592299\n",
      "The lower bound is -45.97597416433446\n",
      "Time taken for lower is 2.4839229583740234\n",
      "Time taken for upper is 0.4610598087310791\n",
      "The lower bound is -46.88839765854308\n",
      "Time taken for lower is 2.5604629516601562\n",
      "Time taken for upper is 0.3786919116973877\n",
      "The upper bound is -46.8883794955076\n"
     ]
    }
   ],
   "source": [
    "threshold = 3\n",
    "[z_low,z_up,final_config] = get_groundBandB(variables,substitutions,hamiltonian,\n",
    "                                            cliques,threshold,\n",
    "                                            solverparameters,verbose = 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Chimera ferromagnetic Ising model with random mangetic field\n",
    "\n",
    "We strat by generating the spin variables, this time distributed according to a Chimera lattice composed of $K_{4,4}$ cells. We make use of the networkx lattice generator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "l = 3\n",
    "k = 3\n",
    "graph = dnx.chimera_graph(l, k, 4)\n",
    "configuration = [2]\n",
    "s_variables = get_lattice_spins(graph, configuration)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then choose an instance of random local fields and define the hamiltonian"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-A0*B0 - A0*C0 - A0*D0 - A0*E0 - A0*I0 - 2.48850589641633*A0 - B0*F0 - B0*G0 - B0*H0 - B0*Z0 - 1.02446459371216*B0 - C0*F0 - C0*G0 - C0*H0 - C0*[0 + 1.75916034102858*C0 - D0*F0 - D0*G0 - D0*H0 - D0*\\0 - 0.68823618976674*D0 - E0*F0 - E0*G0 - E0*H0 - E0*]0 + 0.992063285724269*E0 - F0*N0 + 0.910421858075715*F0 - G0*O0 + 1.46740817470855*G0 - H0*P0 + 0.100173533562314*H0 - I0*J0 - I0*K0 - I0*L0 - I0*M0 - I0*Q0 - 3.26643029709155*I0 - J0*N0 - J0*O0 - J0*P0 - J0*b0 - 1.11127579521358*J0 - K0*N0 - K0*O0 - K0*P0 - K0*c0 + 0.421670177786442*K0 - L0*N0 - L0*O0 - L0*P0 - L0*d0 - 1.38328110160628*L0 - M0*N0 - M0*O0 - M0*P0 - M0*e0 - 1.48425203388179*M0 - N0*V0 + 0.918755754432935*N0 - O0*W0 + 0.37906267868698*O0 - P0*X0 - 0.603892777678683*P0 - Q0*R0 - Q0*S0 - Q0*T0 - Q0*U0 - 1.27344264303068*Q0 - R0*V0 - R0*W0 - R0*X0 - R0*j0 - 0.374741452246733*R0 - S0*V0 - S0*W0 - S0*X0 - S0*k0 + 3.66684912134111*S0 - T0*V0 - T0*W0 - T0*X0 - T0*l0 - 1.53122709699823*T0 - U0*V0 - U0*W0 - U0*X0 - U0*m0 + 2.48113250502132*U0 - 0.757621501559236*V0 + 0.18745424537704*W0 - 0.350173201007192*X0 - Y0*Z0 - Y0*[0 - Y0*\\0 - Y0*]0 - Y0*a0 - 2.59079038574422*Y0 - Z0*^0 - Z0*_0 - Z0*`0 - Z0*r0 + 0.350967532264365*Z0 - [0*^0 - [0*_0 - [0*`0 - [0*s0 + 0.962839970724963*[0 - \\0*^0 - \\0*_0 - \\0*`0 - \\0*t0 - 0.933689551572648*\\0 - ]0*^0 - ]0*_0 - ]0*`0 - ]0*u0 + 0.0435094611976233*]0 - ^0*f0 + 1.80479358813573*^0 - _0*g0 - 0.968667786806358*_0 - `0*h0 + 1.77190497976805*`0 - a0*b0 - a0*c0 - a0*d0 - a0*e0 - a0*i0 - 0.3807346450439*a0 - b0*f0 - b0*g0 - b0*h0 - b0*z0 - 1.23854327076954*b0 - c0*f0 - c0*g0 - c0*h0 - c0*{0 + 2.13347382443256*c0 - d0*f0 - d0*g0 - d0*h0 - d0*|0 + 2.03452302092951*d0 - e0*f0 - e0*g0 - e0*h0 - e0*}0 - 0.0178976398693584*e0 - f0*n0 - 1.02407320033552*f0 - g0*o0 - 2.60084428187098*g0 - h0*p0 + 0.118827424840294*h0 - i0*j0 - i0*k0 - i0*l0 - i0*m0 + 0.716172598159784*i0 - j0*n0 - j0*o0 - j0*p0 - j0*0 - 1.83128560182426*j0 - k0*n0 - k0*o0 - k0*p0 - k0*0 + 1.00638282006691*k0 - l0*n0 - l0*o0 - l0*p0 - l0*0 + 1.25535854114579*l0 - m0*n0 - m0*o0 - m0*p0 - m0*",
      "0 + 1.22420538814843*m0 + 3.97771217923988*n0 - 1.21317558813989*o0 - 0.678644729800935*p0 - q0*r0 - q0*s0 - q0*t0 - q0*u0 - q0*y0 + 2.28016784708288*q0 - r0*v0 - r0*w0 - r0*x0 + 1.91755060160381*r0 - s0*v0 - s0*w0 - s0*x0 + 0.473741842967535*s0 - t0*v0 - t0*w0 - t0*x0 + 1.65720102092551*t0 - u0*v0 - u0*w0 - u0*x0 + 2.34262333125348*u0 - v0*~0 + 2.68657530867914*v0 - w0*0 + 0.771603351271013*w0 - x0*0 - 0.743063444744333*x0 - y0*z0 - y0*{0 - y0*|0 - y0*}0 - y0*0 - 0.479073458400911*y0 - z0*~0 - z0*0 - z0*0 - 1.73626810047589*z0 - {0*~0 - {0*0 - {0*0 - 1.84029922813385*{0 - |0*~0 - |0*0 - |0*0 + 1.09537336445871*|0 - }0*~0 - }0*0 - }0*0 + 2.39590214077158*}0 - ~0*0 + 2.03658789784896*~0 - 0*0 - 0.148452357982056*0 - 0*0 + 0.568166880179667*0 - 0*0 - 0*0 - 0*0 - 0*",
      "0 + 0.635315638958389*0 - 0*0 - 0*0 - 0*0 - 2.1160500253*0 - 0*0 - 0*0 - 0*0 - 0.822815885666396*0 - 0*0 - 0*0 - 0*0 - 0.344387229881653*0 - ",
      "0*0 - ",
      "0*0 - ",
      "0*0 + 1.79529978473877*",
      "0 + 0.139887990096161*0 - 1.69299279959459*0 + 1.17290800576746*0\n"
     ]
    }
   ],
   "source": [
    "h = get_lattice_localdisorder(graph,sigma)\n",
    "J = get_lattice_ferromangetic(graph)   \n",
    "hamiltonian = get_lattice_hamiltonian(graph,h,J,s_variables)\n",
    "print(hamiltonian)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Extract the cliques from the sparsity structure of $\\mathcal{H}$ with the chordal extension method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[I0, J0, N0, O0, P0, b0], [I0, K0, N0, O0, P0, c0], [I0, L0, N0, O0, P0, d0], [I0, M0, N0, O0, P0, e0], [b0, y0, z0, ~0, 0, 0], [c0, y0, {0, ~0, 0, 0], [d0, y0, |0, ~0, 0, 0], [e0, y0, }0, ~0, 0, 0], [I0, Q0, R0, S0, T0, U0], [N0, R0, S0, T0, U0, V0], [O0, R0, S0, T0, U0, W0], [P0, R0, S0, T0, U0, X0], [I0, N0, O0, P0, R0, S0, T0, U0, m0], [I0, N0, O0, P0, R0, S0, T0, l0, m0], [I0, N0, O0, P0, R0, S0, k0, l0, m0], [I0, N0, O0, P0, R0, j0, k0, l0, m0], [a0, i0, j0, k0, l0, m0], [f0, j0, k0, l0, m0, n0], [g0, j0, k0, l0, m0, o0], [h0, j0, k0, l0, m0, p0], [y0, 0, 0, 0, 0, ",
      "0], [~0, 0, 0, 0, ",
      "0, 0], [0, 0, 0, 0, ",
      "0, 0], [0, 0, 0, 0, ",
      "0, 0], [m0, y0, ~0, 0, 0, 0, 0, 0, ",
      "0], [l0, m0, y0, ~0, 0, 0, 0, 0, 0], [k0, l0, m0, y0, ~0, 0, 0, 0, 0], [j0, k0, l0, m0, y0, ~0, 0, 0, 0], [I0, N0, O0, P0, a0, f0, g0, h0, j0, k0, l0, m0, y0, ~0, 0, 0], [A0, B0, C0, D0, E0, I0], [B0, C0, D0, E0, F0, N0], [B0, C0, D0, E0, G0, O0], [B0, C0, D0, E0, H0, P0], [B0, C0, D0, E0, I0, N0, O0, P0, ]0], [B0, C0, D0, I0, N0, O0, P0, \\0, ]0], [B0, C0, I0, N0, O0, P0, [0, \\0, ]0], [B0, I0, N0, O0, P0, Z0, [0, \\0, ]0], [Y0, Z0, [0, \\0, ]0, a0], [Z0, [0, \\0, ]0, ^0, f0], [Z0, [0, \\0, ]0, _0, g0], [Z0, [0, \\0, ]0, `0, h0], [q0, r0, s0, t0, u0, y0], [r0, s0, t0, u0, v0, ~0], [r0, s0, t0, u0, w0, 0], [r0, s0, t0, u0, x0, 0], []0, r0, s0, t0, u0, y0, ~0, 0, 0], [\\0, ]0, r0, s0, t0, y0, ~0, 0, 0], [[0, \\0, ]0, r0, s0, y0, ~0, 0, 0], [Z0, [0, \\0, ]0, r0, y0, ~0, 0, 0], [I0, N0, O0, P0, Z0, [0, \\0, ]0, a0, f0, g0, h0, y0, ~0, 0, 0], [I0, N0, O0, P0, a0, b0, c0, d0, e0, f0, g0, h0, y0, ~0, 0, 0]]\n"
     ]
    }
   ],
   "source": [
    "variables = [el[0][0] for el in s_variables.values()]\n",
    "cliques = find_variable_cliques(variables, hamiltonian)\n",
    "print(cliques)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set a threshold size $n_t$ for the hybrid level of the hierarcy and run the CBB"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Starting initial optimisation\n"
     ]
    }
   ],
   "source": [
    "threshold = 5\n",
    "[z_low,z_up,final_config] = get_groundBandB(variables,substitutions,hamiltonian,\n",
    "                                            cliques,threshold,\n",
    "                                            solverparameters,verbose = 0)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
